﻿using Server.Models;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Server.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "get")]
    public class CertificatesController : ApiController
    {
        // GET api/certificates 
        public IHttpActionResult Get()
        {
            List<Certificate> results = new List<Certificate>();
            using (CertificatesManager cm = new CertificatesManager())
            {
                results.AddRange(cm.GetAll());
            }

            return Ok(results);
        }

        // GET api/certificates/5 
        public IHttpActionResult Get(int id)
        {

            Certificate result = new Certificate();
            using (CertificatesManager cm = new CertificatesManager())
            {
                var certs = cm.Get(id);
                result = certs;
            }

            if(result == null)
            {
                return NotFound();
            }

            return Ok(result); ;
        }
    }
}
