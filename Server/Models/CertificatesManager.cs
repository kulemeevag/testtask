﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Server.Models
{
    public class CertificatesManager : IDisposable
    {
        X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);

        public CertificatesManager()
        {
            store.Open(OpenFlags.ReadOnly);
        }

        public List<Certificate> GetAll()
        {
            List<Certificate> results = new List<Certificate>();
            foreach (var cert in store.Certificates)
            {
                Certificate temp = new Certificate()
                {
                    Version = cert.Version,
                    SerialNumber = cert.SerialNumber,
                    SignatureAlgorithm = cert.SignatureAlgorithm.FriendlyName,
                    Issuer = cert.Issuer,
                    ValidFrom = cert.NotBefore,
                    ValidTo = cert.NotAfter,
                    SignatureHashAlgorithm = cert.PublicKey.Key.KeyExchangeAlgorithm,
                    Subject = cert.Subject,
                    PublicKey = String.Format("{0} ({1} Bits)", cert.PublicKey.Oid.FriendlyName, cert.PublicKey.Key.KeySize)
                };
                results.Add(temp);
            }
            return results;
        }

        public Certificate Get(int id)
        {
            int i = 1;
            Certificate result = null;
            foreach (var cert in store.Certificates)
            {
                if (id == i)
                {
                    Certificate temp = new Certificate()
                    {
                        Version = cert.Version,
                        SerialNumber = cert.SerialNumber,
                        SignatureAlgorithm = cert.SignatureAlgorithm.FriendlyName,
                        Issuer = cert.Issuer,
                        ValidFrom = cert.NotBefore,
                        ValidTo = cert.NotAfter,
                        SignatureHashAlgorithm = cert.PublicKey.Key.KeyExchangeAlgorithm,
                        Subject = cert.Subject,
                        PublicKey = String.Format("{0} ({1} Bits)", cert.PublicKey.Oid.FriendlyName, cert.PublicKey.Key.KeySize)
                    };
                    result = temp;
                }
                i++;
            }
            return result;
        }

        public void Dispose()
        {
            store.Close();
        }
    }
}
