﻿using System;

namespace Server.Models
{
    public class Certificate
    {
        public int Version { get; set; }
        public string SerialNumber { get; set; }
        public string SignatureAlgorithm { get; set; }
        public string SignatureHashAlgorithm { get; set; }
        public string Issuer { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public string Subject { get; set; }
        public string PublicKey { get; set; }
    }
}
