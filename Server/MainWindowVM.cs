﻿using Microsoft.Owin.Hosting;
using Server.Models;
using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Windows.Input;

namespace Server
{
    public class MainWindowVM
    {
        public ObservableCollection<Certificate> Items { get; set; }
        public Certificate SelectedCertificate { get; set; }
        private IDisposable _webApp;

        string ServerURI = ConfigurationManager.AppSettings["ServerURI"].ToString();
        const string pathToLog = "Log";
        const string fileLog = "log.txt";
        const string writePath = pathToLog + @"\" + fileLog;

        #region Commands
        public ICommand CloseCmd { get; set; }
        #endregion

        public MainWindowVM()
        {
            CloseCmd = new Command(arg => StopServer());
            Items = new ObservableCollection<Certificate>();
            using (CertificatesManager cm = new CertificatesManager())
            {
                foreach (var cert in cm.GetAll())
                {
                    Items.Add(cert);
                }
            }

            WriteToConsole("Starting web Server...");
            _webApp = WebApp.Start<Startup>(url: ServerURI);
            WriteToConsole(String.Format("Server running at {0}", ServerURI));
        }

        /// <summary>
        /// Stop server
        /// </summary>
        private void StopServer()
        {
            WriteToConsole("Server is closed");
            _webApp.Dispose();
            App.Current.Shutdown();
        }

        /// <summary>
        /// Write to log file
        /// </summary>
        /// <param name="message"></param>
        public void WriteToConsole(String message)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(pathToLog);
            if (!dirInfo.Exists)
            {
                dirInfo.Create();
                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(DateTime.Now + " " + message);
                }
            }
            else
            {
                using (StreamWriter sw = new StreamWriter(writePath, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine(DateTime.Now + " " + message);
                }
            }
        }
    }
}
