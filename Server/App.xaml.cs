﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;

namespace Server
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        System.Threading.Mutex mutex;
        private void App_Startup(object sender, StartupEventArgs e)
        {
            bool createdNew;
            string guid = Marshal.GetTypeLibGuidForAssembly(Assembly.GetExecutingAssembly()).ToString();
            mutex = new System.Threading.Mutex(true, guid, out createdNew);
            if (!createdNew)
            {
                MessageBox.Show("Already running another copy.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Thread.Sleep(3000);
                return;
            }
        }
    }
}
